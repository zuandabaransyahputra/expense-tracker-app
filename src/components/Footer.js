const Footer = () => {
  return (
    <footer id="footer" className="text-black py-5">
      <div className="container">
        <div className="row">
          <div className="col text-center">
            <p className="lead mb-0">Expense Tracker</p>
            <p>Catat setiap pemasukan dan pengeluaranmu.</p>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Footer;